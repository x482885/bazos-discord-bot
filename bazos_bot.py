#!/usr/bin/env python3

import sys
import os
import datetime
import requests
import logging
import discord
from discord import Colour
from discord.ext import commands
from discord.ext import tasks
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from listing import Listing
from dotenv import load_dotenv

########################################################################################################################
# set up constants
########################################################################################################################
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
CHANNEL_ID = int(os.getenv('CHANNEL_ID'))
URL = "https://pc.bazos.sk/graficka/?cenaod=150&cenado=400"

########################################################################################################################
# set up logger
########################################################################################################################
logger = logging.getLogger("logger")
logging.basicConfig(filename='logger.log', filemode='w', format='%(asctime)s :: %(levelname)-8s :: %(message)s')
logger.setLevel(logging.DEBUG)

def log_unexpected_exception(exc_type, exc_value, exc_traceback):
    logger.critical("Uncaught exception!", exc_info=(exc_type, exc_value, exc_traceback))


sys.excepthook = log_unexpected_exception
########################################################################################################################
# set up other variables
########################################################################################################################
client = commands.Bot(command_prefix='!', intents=discord.Intents.default())
channel = None
listings_of_day = []
current_date = (datetime.today() - timedelta(1)).date()
nvidia_keywords = ["nvidia", "geforce", "gt", "gtx", "rtx"]
amd_keywords = ["amd", "radeon", "sapphire", "rx", "r3", "r5", "r7", "r9"]
intel_keywords = ["intel", "arc"]
site_down = False
blacklist_filter = set()


@client.event
async def on_ready():
    global channel
    channel = client.get_channel(CHANNEL_ID)
    if channel is None:
        logger.critical(f"Channel with id {CHANNEL_ID} not found!")
        return
    logger.info(
        f"Program started with: TOKEN={TOKEN}, CHANNEL_ID={CHANNEL_ID}, URL={URL}, bot_prefix={client.command_prefix}, channel={channel.name}, date={current_date}, nvidia=[{','.join(nvidia_keywords)}, amd=[{','.join(amd_keywords)}, intel=[{','.join(intel_keywords)}, site_down={site_down}, blacklist=[{'None' if len(blacklist_filter) == 0 else ','.join(blacklist_filter)}]")
    bazos_check.start()


async def delete_messages():
    logger.debug('Starting method delete_messages')
    deleted = await channel.purge(limit=None, oldest_first=True, before=datetime.now() - timedelta(14))
    logger.info(f"Number of messages deleted: {len(deleted)}")


@client.event
async def on_command_completion(ctx):
    temp_ctx = ctx
    message = ctx.message
    await message.delete()
    logger.debug(
        f"Message deleted: created_at={message.created_at}, author={message.author}, command={temp_ctx.command.name}, message={message.content}")


@tasks.loop(seconds=30, count=None)
async def bazos_check():
    global current_date
    if current_date != datetime.today().date():
        temp_date = current_date
        current_date = datetime.today().date()
        logger.info(f"Date changed from {temp_date} to {current_date}")
        await delete_messages()
        listings_of_day.clear()
        logger.debug(f"Listings of the day cleared")
    if len(listings_of_day) == 0:
        logger.debug("Starting listings method, initiated by empty listings list")
        await listings(channel)
    else:
        logger.debug("Starting adder method, initiated as time event")
        await adder(True)


async def adder(notification, tempURL=URL):
    logger.info(f"Adder started: notification={notification}, tempURL={tempURL}")
    global listings_of_day
    global site_down
    try:
        soup = BeautifulSoup(requests.get(tempURL).content, "html.parser")
        site_down = False
    except:
        if site_down: return
        logger.exception("Site down!", exc_info=True, stack_info=True)
        await channel.send("Site down!")
        site_down = True
        return
    listings_divs = soup.find_all("div", class_="inzeraty")
    logger.debug(f"Found {len(listings_divs)} listings")
    for listing_div in listings_divs:
        date = datetime.strptime(listing_div.find_next(class_="velikost10").text.strip().split('[')[1].split(']')[0],
                                 '%d.%m. %Y').date()
        isTop = listing_div.find_next(class_="ztop") is not None

        if date != current_date:
            logger.debug(f"Listing: date={date}, isTop={isTop} <> current date={current_date}")
            if isTop:
                continue
            break

        number = listing_div.find_next("a")['href'].split("/")[2]
        title = listing_div.find_next(class_="nadpis").text.strip()
        description = listing_div.find_next(class_="popis").text.strip()
        price = listing_div.find_next(class_="inzeratycena").text.strip()
        location = listing_div.find_next(class_="inzeratylok").text.strip()
        views = listing_div.find_next(class_="inzeratyview").text.strip()
        picture = listing_div.find_next(class_="obrazek")['src']
        link = "https://pc.bazos.sk" + listing_div.find_next("a")['href']

        if any(word in title for word in blacklist_filter):
            logger.debug(f"Listing filtered! title={title}, blacklist=[{','.join(blacklist_filter)}]")
            continue

        listing = Listing(number, date, isTop, title, description, price, location, views, picture, link)

        if listing not in listings_of_day:
            if all(item.link != listing.link for item in listings_of_day):
                listings_of_day.append(listing)
                logger.debug("Listing added to listings of the day")
            else:
                for item in listings_of_day.copy():
                    if item.link == listing.link:
                        item.update(listing)
                        listing = item
                        break
            if notification:
                logger.debug("Attempting to send notification...")
                await send_notification(listing)

        if listing_div == listings_divs[-1]:
            next_page_link = soup.find(class_="strankovani").find_all("a")[-1]
            if "Ďalšia" in next_page_link.text:
                nextURL = "https://pc.bazos.sk" + next_page_link['href']
                logger.debug(
                    f"Starting adder method, initiated by next page redirecting (recursive): nextURL={nextURL}")
                await adder(notification, nextURL)


async def send_notification(listing):
    title = listing.title.lower()
    if any(ele in title for ele in amd_keywords):
        color = Colour.red()
    elif any(ele in title for ele in nvidia_keywords):
        color = Colour.green()
    elif any(ele in title for ele in intel_keywords):
        color = Colour.blue()
    else:
        color = Colour.blurple()

    embed_var = discord.Embed(title=listing.title, description=f"```{listing.description}```",
                              colour=color,
                              url=listing.link)
    embed_var.add_field(name="Price", value=listing.price, inline=True)
    embed_var.add_field(name="Location", value=listing.location, inline=True)
    if listing.latest_update is not None:
        embed_var.add_field(name="Updated", value=listing.latest_update)
    embed_var.set_thumbnail(url=listing.picture)

    await channel.send(embed=embed_var)
    logger.info(f"Notification sent: color={color}, updated={True if listing.latest_update is not None else False}")


@client.command(name='listings', help='Get list of today\'s listings')
async def listings(ctx):
    logger.debug("Starting method listings(command)")
    log_listings_of_day(True)
    listings_of_day.clear()
    logger.debug("Starting adder method, initiated as part of listings command invocation")
    await adder(False)
    log_listings_of_day(False)
    embed_var = discord.Embed(title="Listings of the day", colour=Colour.blurple(), url=URL)
    for listing in listings_of_day:
        if listing.isTop:
            embed_var.add_field(name="[TOP] - " + listing.title, value=listing.price + f" - [{listing.views}]")
        else:
            embed_var.add_field(name=listing.title, value=listing.price + f" - [{listing.views}]")
    await channel.send(embed=embed_var)
    logger.debug(f"Listings of the day sent, size={len(listings_of_day)}")


@client.command(name='range',
                help='Change the range of listings, takes input 2 number parameters (e.g. !range 200 500)')
async def price_range(ctx, arg1, arg2):
    logger.debug(f"Starting method price_range(command)")
    try:
        int(arg1)
        int(arg2)
    except:
        await channel.send("Invalid arguments!")
        logger.warning(f"Invalid arguments: author={ctx.message.author}, message={ctx.message.content}", exc_info=True)
        return
    global URL
    URL = f"https://pc.bazos.sk/graficka/?cenaod={arg1}&cenado={arg2}"
    await channel.send(
        embed=discord.Embed(title=f"New range set to {arg1} - {arg2}", url=URL, colour=Colour.dark_grey()))
    logger.debug(f"Price range set and message sent: rangeLow={str(arg1)}, rangeHigh={str(arg2)}, URL={URL}")


@client.command(name='filter-add', help='Adds desired word to blacklist, takes 1 input parameter. (e.g. !filter-add 4070)')
async def add_filter(ctx, arg1):
    logger.debug(f"Starting method add_filter(command)")
    try:
        keyword = str(arg1).strip()
    except:
        await channel.send("Invalid arguments!")
        logger.warning(f"Invalid arguments: author={ctx.message.author}, message={ctx.message.content}", exc_info=True)
        return
    if keyword not in blacklist_filter:
        blacklist_filter.add(keyword)
        title_var = f"Keyword \"{keyword}\" added to blacklist."
    else:
        title_var = f"Keyword \"{keyword}\" already blacklisted."
    await channel.send(
        embed=discord.Embed(title=title_var, colour=Colour.blurple()))
    logger.debug(f"Message sent: content={title_var}")
    await show_filter(ctx)


@client.command(name='filter-remove', help='Removes desired word from blacklist, takes 1 input parameter. (e.g. '
                                           '!filter-remove 4070)')
async def remove_filter(ctx, arg1):
    logger.debug(f"Starting method remove_filter(command)")
    try:
        keyword = str(arg1).strip()
    except:
        await channel.send("Invalid arguments!")
        logger.warning(f"Invalid arguments: author={ctx.message.author}, message={ctx.message.content}", exc_info=True)
        return
    logger.debug(f"Keyword={keyword}")
    if keyword not in blacklist_filter:
        title_var = f"Keyword \"{keyword}\" not blacklisted."
    else:
        blacklist_filter.remove(keyword)
        title_var = f"Keyword \"{keyword}\" removed from blacklist."
    await channel.send(
        embed=discord.Embed(title=title_var, colour=Colour.blurple()))
    logger.debug(f"Message sent: content={title_var}")
    await show_filter(ctx)


@client.command(name='filter', help='Shows blacklisted/filtered keywords.')
async def show_filter(ctx):
    logger.debug(f"Starting method show_filter(command): size of blacklist={len(blacklist_filter)}")
    if len(blacklist_filter) == 0:
        embed_var = discord.Embed(title=f"Blacklist is empty", colour=Colour.blurple())
    else:
        embed_var = discord.Embed(title=f"Blacklist", description=', '.join(blacklist_filter),
                                  colour=Colour.blurple())
    await channel.send(embed=embed_var)
    logger.info(
        f"Filter shown - size:{len(blacklist_filter)} - items:{'None' if len(blacklist_filter) == 0 else ', '.join(blacklist_filter)}")

@client.command(name='log', help='Print the contents of the logger')
async def print_log(ctx):
    try:
        logger.debug(f"Starting method print_log(command)")
        await channel.send(file=discord.File(fp='logger.log', filename='logger.txt'), content='Logger Content:')
    except:
        logger.warning(f"Logger file not found: {f.name}", exc_info=True)
        await channel.send('Logger file not found.')

def log_listings_of_day(before):
    strings_listings_of_day = '\n'.join([repr(item) for item in listings_of_day])
    logger.debug(
        f"Listings of day {'before' if before else 'after'} adder ({len(listings_of_day)}):{strings_listings_of_day}")


client.run(TOKEN)

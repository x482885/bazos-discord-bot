# Bazos discord bot

Bazos discord for sending notifications about new listings of graphic cards

Commands:

- !help : shows available commands with description
- !listings : updates and shows current listings of the day
- !range <min> <max> : takes two int arguments to set price range of the listings
- !filter : shows blacklisted/filtered keywords
- !filter-add <keyword> : adds desired word (1 input parameter) to blacklist
- !filter-remove <keyword> : removes desired word (1 input parameter) from blacklist
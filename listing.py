import logging

logger = logging.getLogger('logger')


class Listing:
    latest_update = []

    def __init__(self, number, date, isTop, title, description, price, location, views, picture, link):
        self.number = number
        self.date = date
        self.isTop = isTop
        self.title = title
        self.description = description
        self.price = price
        self.location = location
        self.views = views
        self.picture = picture
        self.link = link
        logger.debug(f"Listing created: listing={repr(self)}")

    def update(self, listing):
        updated = []
        self.latest_update = None

        if self.isTop != listing.isTop: updated.append("isTop")
        if self.description != listing.description: updated.append("description")
        if self.price != listing.price: updated.append("price")
        if self.location != listing.location: updated.append("location")
        if self.picture != listing.picture: updated.append("picture")

        if len(updated) == 0: return

        self.date = listing.date
        self.isTop = listing.isTop
        self.title = listing.title
        self.description = listing.description
        self.price = listing.price
        self.location = listing.location
        self.views = listing.views
        self.picture = listing.picture

        self.latest_update = ','.join(updated)

        logger.debug(f"Listing updated to: {repr(self)}")

    def __eq__(self, other):
        if not isinstance(other, Listing): return False

        return self.number == other.number and \
            self.date == other.date and \
            self.isTop == other.isTop and \
            self.title == other.title and \
            self.description == other.description and \
            self.price == other.price and \
            self.location == other.location and \
            self.picture == other.picture and \
            self.link == other.link

    def __repr__(self):
        updated = "None" if len(self.latest_update) == 0 else ','.join(self.latest_update)
        return f'Listing({",".join([str(self.number), str(self.date), str(self.isTop), str(self.title), str(self.price), str(self.location), str(self.views), str(self.picture), str(self.link), f"Updated:{updated}"])})'